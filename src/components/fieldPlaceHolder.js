import React from 'react'
import { Input } from 'reactstrap'

export default ({ input, label, className, type, meta: { touched, error } }) => {
  const fullClass = (className?className:"") + ((touched && error)?" error":"")
  return (
    <div className={fullClass}>
      <Input {...input} type={type} placeholder={label} />
      {(touched && error) && <span className="error-message">{error}</span>}
    </div>
  )
}

