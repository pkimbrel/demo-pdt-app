import React, { Component } from 'react'

import { NavLink } from 'react-router-dom'
import { Button, Nav, NavItem } from 'reactstrap'

import * as auth from 'app/auth'

export default
class Header extends Component {
  logout = () => {
    auth.logout()
  }

  render() {
    return (
      <div className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
        <h5 className="my-0 mr-md-auto font-weight-normal"><NavLink className="text-dark" to="/">Demo PDT App</NavLink></h5>
        {auth.isAuthenticated() &&
        <Nav className="my-2 my-md-0 mr-md-3">
          <NavItem className="p-2">
            <NavLink className="text-dark mr-3" to="/page1">Page 1</NavLink>
            <NavLink className="text-dark" to="/page2">Page 2</NavLink>
          </NavItem>
          <Button className="mx-2 d-none d-md-block btn-outline-primary" onClick={this.logout}>Logout</Button>
        </Nav>
      }
      </div>
    )
  }
}