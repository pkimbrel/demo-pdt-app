import React, { Component } from 'react'

export default
class Loading extends Component {
  render() {
    const { isFetching } = this.props
    if (isFetching) {
      return (<span>Loading...</span>)
    } else {
      return this.props.children
    }
  }
}