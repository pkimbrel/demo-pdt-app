import React, { Component } from 'react'

import { Pagination as ReactPagination, PaginationItem, PaginationLink } from 'reactstrap'

const MAX_PAGES = 7

export default
class Pagination extends Component {
  render() {
    const { reloadData, total, pageSize, start } = this.props

    const disablePrevious = (start === 0)
    const disableNext = (start >= (total - pageSize))
    const totalPages = Math.ceil(total / pageSize)
    const activePage = Math.floor(start / pageSize)

    const rangexy = (start, end) => Array.from({length: (end - start)}, (_, k) => k + start)
    const halfway = Math.floor(MAX_PAGES/2)
    
    var pages = []
    if (totalPages < MAX_PAGES) {
      pages = rangexy(0, totalPages)
    } else if (activePage < halfway) {
      const low = 0
      const high = MAX_PAGES
      pages = rangexy(low, (high<MAX_PAGES)?high:MAX_PAGES)
    } else if (activePage >= (totalPages - halfway)) {
      const low = totalPages - MAX_PAGES
      const high = totalPages
      pages = rangexy(low, (high<totalPages)?high:totalPages)
    } else {
      const low = activePage-halfway
      const high = low + MAX_PAGES
      pages = rangexy(low, (high<totalPages)?high:totalPages)
    }

    return (
      <ReactPagination size="sm" aria-label="Pagination">
        <PaginationItem disabled={disablePrevious}>
          <PaginationLink previous onClick={() => reloadData((activePage - 1) * pageSize)}/>
        </PaginationItem>
        {pages.map((index, _) => (
        <PaginationItem active={index === activePage} key={"page" + index}>
          <PaginationLink onClick={() => reloadData(index * pageSize)}>{index+1}</PaginationLink>
        </PaginationItem>
        ))}
        <PaginationItem disabled={disableNext}>
          <PaginationLink next onClick={() => reloadData((activePage + 1) * pageSize)}/>
        </PaginationItem>
      </ReactPagination>
      )
    }
  
}

