import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { Route, BrowserRouter, Switch, Redirect } from 'react-router-dom'
import { createStore, combineReducers, applyMiddleware } from 'redux'
import { reducer as formReducer } from 'redux-form'

import * as login from 'pages/login'

export default (middleware, logPageView) => {
  const store = createStore(
    combineReducers({
      login: login.reducer,
      form: formReducer
    }),
    applyMiddleware.apply(applyMiddleware, middleware)
  )
  
  ReactDOM.render(
    <Provider store={store}>
      <BrowserRouter>
        <div>
          <Route component={logPageView} />
          <Switch>
            <Route path="/login" exact component={login.page} />
            <Redirect from="/*" to="/login"/>
          </Switch>
        </div>
      </BrowserRouter>
    </Provider>,
    document.getElementById('root')
  )
}

