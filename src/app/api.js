import axios from 'axios'

const ENDPOINTS = (() => {
  var currentHost = window.location.hostname

  if (currentHost.indexOf("localhost") > -1) {
    return {
      apiUrl: "http://localhost:3000",
      authUrl: "http://localhost:3000"
    }
  } else if (currentHost.indexOf("test") > -1) {
    return {
      apiUrl: "https://api.test.aws.statefarm",
      authUrl: "https://auth.test.aws.statefarm"
    }
  } else {
    return {
      apiUrl: "https://api.prod.aws.statefarm",
      authUrl: "https://auth.prod.aws.statefarm"
    }
  }
})()

export const get = (url) => {
  let token = localStorage.getItem('token') || null

  return axios.get(ENDPOINTS.apiUrl + url, {
    headers: {
      "Authorization": "Token " + token
    }
  })
}

export const put = (url, data = null) => {
  let token = localStorage.getItem('token') || null

  return axios.put(ENDPOINTS.apiUrl + url, data, {
    headers: {
      "Authorization": "Token " + token
    }
  })
}

export const del = (url, data) => {
  let token = localStorage.getItem('token') || null

  return axios.delete(ENDPOINTS.apiUrl + url, {
    data,
    headers: {
      "Authorization": "Token " + token
    }
  })
}

export const post = (url, data) => {
  let token = localStorage.getItem('token') || null

  return axios.post(ENDPOINTS.apiUrl + url, data, {
    headers: {
      "Authorization": "Token " + token
    }
  })
}

export const _get = (url) => {
  return axios.get(ENDPOINTS.authUrl + url)
}

export const _post = (url, data) => {
  return axios.post(ENDPOINTS.authUrl + url, data)
}

export const _put = (url, data) => {
  let token = localStorage.getItem('token') || null
  return axios.put(ENDPOINTS.authUrl + url, data, {
    headers: {
      "Authorization": "Token " + token
    }
  })
}