const DEFAULT_STATE = (pages) => {
  var state = {}
  pages.forEach((p) => {
    state = {
      ...state,
      [p]: {
        isFetching: false,
        dirty: true
      }
    }
  })
  return state
}

export default (pages) => (state = DEFAULT_STATE(pages), action) => {
  const matches = /(.*)_(DIRTY|PENDING|FULFILLED|FAILED)/.exec(action.type)
  
  if (!matches) return state
  
  const [, s1, method] = matches
  const stateName = s1.toLowerCase()

  return {
    ...state,
    [stateName]: {
      ...state[stateName],
      dirty: method === "DIRTY",
      isFetching: method === 'PENDING'
    }
  }
}



