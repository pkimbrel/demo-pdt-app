export default (pattern) => (value, previousValue) => {
  if (!value) {
    return value
  }

  return value.match(pattern) ? value : previousValue
}