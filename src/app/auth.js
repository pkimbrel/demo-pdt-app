import * as api from 'app/api'
//import jwtDecode from 'jwt-decode'

export const login = (login, password) => {
  var url = "/login-bad.json"
  if (password === "good") {
    url = "/login-good.json"
  }

  return api._get(url).then((result) => {
    let response = result.data.response

    if (response === "ok") {
      let expiration = new Date().getTime() + (15 * 60000)
      localStorage.setItem('token', "Bogus")
      localStorage.setItem('expires_at', expiration)
      window.location="/"
    } else {
      throw new Error("Unable to login")
    }
  })

  /*return api._post("/login", { login, password }).then((result) => {
    let response = result.data.response

    if (response === "ok") {
      let token = result.data.token            
      let expiration = jwtDecode(token).exp * 1000
      localStorage.setItem('token', token)
      localStorage.setItem('expires_at', expiration)
      window.location="/"
    } else if (response === "validate-terms") {
      window.location = "/new-terms"
    } else {
      throw new Error("Unable to login")
    }
  })*/
}

export const refresh = () => {
  return api._get("/login-good.json").then((result) => {
    //let token = result.data.token
    let expiration = new Date().getTime() + (15*60000) //jwtDecode(token).exp * 1000
    localStorage.setItem('token', "Bogus")
    localStorage.setItem('expires_at', expiration)
  }, () => {
    logout()
  })

  /*
  return api._put("/refresh", null).then((result) => {
    let token = result.data.token
    let expiration = jwtDecode(token).exp * 1000
    localStorage.setItem('token', token)
    localStorage.setItem('expires_at', expiration)
  }, () => {
    logout()
  })*/
}

export const logout = () => {
  localStorage.removeItem('token')
  localStorage.removeItem('expires_at')
  sessionStorage.removeItem('spending_filter')
  window.location = "/"
}

export const isAuthenticated = () => {
  let expiresAt = JSON.parse(localStorage.getItem('expires_at'))
  return new Date().getTime() < expiresAt
}
