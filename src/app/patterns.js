export const validLoginID = /^([A-Za-z0-9]){4,4}$/
export const validLoginIDNormalized = /^([A-Za-z0-9]){1,4}$/
export const validPassword = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[&^@#*%$_+={}?,;:]).{8,32}$/
export const validPasswordNormalized = /^([a-zA-Z\d&^@#*%$_+={}?,;:]){1,32}$/
