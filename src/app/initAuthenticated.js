import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { Route, BrowserRouter, Switch } from 'react-router-dom'
import { createStore, combineReducers, applyMiddleware } from 'redux'

import moment from 'moment'
import * as auth from 'app/auth'

import loadManager from 'app/loadManager'

import * as page1 from 'pages/page1'
import * as page2 from 'pages/page2'
import * as pageNotFound from 'pages/pageNotFound'

const pages = ["page1", "page2"]
export default (middleware, logPageView) => {
  const store = createStore(
    combineReducers({
      loadManager: loadManager(pages),
      page1: page1.reducer,
      page2: page2.reducer
    }),
    applyMiddleware.apply(applyMiddleware, middleware)
  )
  
  /******************************/
  /* Auto auth refresh / logout */
  /******************************/

  var lastUpdated = moment()
  window.addEventListener('scroll', () => { lastUpdated = moment() })
  document.getElementById('root').addEventListener('mousemove', () => { lastUpdated = moment() })
  const pageLoaded = () => {
    lastUpdated = moment()
    return null
  }  
  
  const timeout = 15 * 60000;
  setInterval(() => {
    if (auth.isAuthenticated()) {
      if (moment().diff(lastUpdated) < (timeout - 1000)) {
        auth.refresh()
      } else {
        auth.logout()
      }
    }
  }, timeout)
  
  /******************************/
  /******************************/

  ReactDOM.render(
    <Provider store={store}>
      <BrowserRouter>
        <div>
          <Route component={pageLoaded} />
          <Route component={logPageView} />
          <Switch>
            <Route path="/" exact component={page1.page} />
            <Route path="/page1" exact component={page1.page} />
            <Route path="/page2" exact component={page2.page} />
            <Route component={pageNotFound.page}/>
          </Switch>
        </div>
      </BrowserRouter>
    </Provider>,
    document.getElementById('root')
  )
}

