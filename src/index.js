import thunk from 'redux-thunk';
import logging from 'redux-logger'
import promise from 'redux-promise-middleware'

import * as auth from 'app/auth'
import initAuthenticated from 'app/initAuthenticated'
import initUnauthenticated from 'app/initUnauthenticated'

import 'bootstrap/dist/css/bootstrap.css'
import './index.scss'

const logPageView = () => {
  console.log("Logging page view: " + window.location.pathname)
  return null
}

const middleware = [ promise(), thunk ]
if (process.env.NODE_ENV !== 'production') {
  middleware.push(logging)
}

if (!auth.isAuthenticated()) {
  initUnauthenticated(middleware, logPageView);
} else {
  initAuthenticated(middleware, logPageView)
}




