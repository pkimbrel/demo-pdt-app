const DEFAULT_STATE = {
  response: ""
}

export const reducer = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case "PAGE1_FULFILLED":
      return {
        ...state,
        response: action.payload.data.response
      }
    default:
      return state
  }
}
