import * as api from 'app/api'

export const reload = () => (dispatch) => {
  return dispatch({
    type: "PAGE1",
    payload: api.get("/page1.json")
  })
}
