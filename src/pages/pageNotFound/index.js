import React, { Component } from 'react';

import { Container, Row, Col } from 'reactstrap'

import Header from 'components/header'

class NoMatch extends Component {
  render() {
    return (
      <div>
        <Header />
        <Container>
        <Row>
          <Col sm="4">
            404 - Not Found
          </Col>
        </Row>
        </Container>
      </div>
    );
  }
}

export const page = NoMatch
