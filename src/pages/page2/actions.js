import * as api from 'app/api'

export const reload = () => (dispatch) => {
  return dispatch({
    type: "PAGE2",
    payload: api.get("/page2.json")
  })
}
