import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Container, Row, Col } from 'reactstrap'

import Header from 'components/header'
import Loading from 'components/loading'

import * as actions from './actions'

const mapStateToProps = (state) => {
  return {
    ...state.page2,
    ...state.loadManager.page2}
}

class Page extends Component {
  componentDidMount() {
    const { dirty, dispatch } = this.props
    if (dirty) {
      dispatch(actions.reload())
    }
  }

  render() {
    const { response, isFetching } = this.props

    return (
      <div>
        <Header />
        <Container className="dashboard">
        <Row>
          <Col sm="12">
            <Loading isFetching={isFetching}>
              <h2>Page 2</h2>
              Server Response: {response}
            </Loading>
          </Col>
        </Row>
        </Container>
      </div>
    );
  }
}

export const page = connect(mapStateToProps)(Page)
export * from './reducer'
export * from './actions'

