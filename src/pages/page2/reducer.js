const DEFAULT_STATE = {
  response: ""
}

export const reducer = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case "PAGE2_FULFILLED":
      return {
        ...state,
        response: action.payload.data.response
      }
    default:
      return state
  }
}
