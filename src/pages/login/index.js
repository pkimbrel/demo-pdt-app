import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'

import { Container, Row, Col, Form, FormGroup, Button } from 'reactstrap'

import fieldPlaceHolder from 'components/fieldPlaceHolder'
import Header from 'components/header'

import normalizeText from 'app/normalizeText'
import * as patterns from 'app/patterns'

import validate from './validate'
import { submitLogin } from './actions'

import "./style.scss"

const mapStateToProps = (state) => {
  var errors = (state.form.login && state.form.login.syncErrors) || {}
  var submitError = (state.form.login && 
                     state.form.login.submitErrors &&
                     state.form.login.submitErrors.submit) ? state.form.login.submitErrors.submit : null
  
  return {
    initialValues: state.login,
    submitError,
    hasErrors: (Object.keys(errors).length > 0)
  }
}

class LoginPage extends Component {
  render() {
    const { submitError, hasErrors, handleSubmit, submitting, pristine } = this.props
    const disableSubmit = pristine || submitting || hasErrors

    return (
      <div>
        <Header />
        <Container>
          <Row>
            <Col md={{ size: '4', offset: 4 }} className="login-form">
              <h3>Login</h3>
              <Form onSubmit={handleSubmit(submitLogin)}>
                <FormGroup>
                  <Field name="login" 
                    component={fieldPlaceHolder}
                    label="Login ID"
                    normalize={normalizeText(patterns.validLoginIDNormalized)} />
                </FormGroup>
                <FormGroup>
                  <Field name="password" 
                      component={fieldPlaceHolder}
                      type="password"
                      label="Password"
                      normalize={normalizeText(patterns.validPasswordNormalized)} />
                </FormGroup>
                {submitError && 
                  <div className="form-group error-message">
                    {submitError}
                  </div>
                }
                <FormGroup>
                  <Button outline type="submit" color="primary" disabled={disableSubmit}>Login</Button>
                </FormGroup>
              </Form>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

var form = reduxForm({
  enableReinitialize: true,
  form: 'login',
  validate
})(LoginPage)

export * from './reducer'
export * from './actions'

export const page = connect(mapStateToProps)(form)

