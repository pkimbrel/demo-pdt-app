import * as patterns from 'app/patterns';

export default values => {
  var errors = {}

  if (!values.login || !values.login.match(patterns.validLoginID)) {
    errors = {...errors, login: "Invalid login ID"}
  } else {
    if (values.login.length < 4) {
      errors = {...errors, login: "Login too short"}
    }
  
    if (values.login.length > 4) {
      errors = {...errors, login: "Login too long"}
    }
  }

  /*if (!values.password || !values.password.match(patterns.validPassword)) {
    errors = {...errors, password: "Invalid password"}
  } else  {
    if (values.password.length < 8) {
      errors = {...errors, password: "Password too short"}
    }
  
    if (values.password.length > 32) {
      errors = {...errors, password: "Password too long"}
    }
  }*/

  return errors
}