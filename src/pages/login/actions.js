import { SubmissionError } from 'redux-form'
import * as auth from 'app/auth'

export const submitLogin = (values) => {
  return auth.login(
    values.login,
    values.password)
  .catch((e) => {
    var message = "Unable to login"
    if (e && e.response && e.response.data && e.response.data.message) {
      message = e.response.data.message
    }
    throw new SubmissionError({submit: message})
  })
}
